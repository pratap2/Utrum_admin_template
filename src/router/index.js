import Vue from 'vue'
import Router from 'vue-router'
import dashboard from '@/components/dashboard'
import searchbar from '@/components/Searchbar/searchbar'
import useravatar from '@/components/UserAvatar/useravatar'
import topbarsetting from '@/components/TopbarSetting/topbar-setting'
import topbarnotification from '@/components/TopbarNotification/topbar-notification'



Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Dashboard',
      component: dashboard
    },
    {
      path: '/searchbar',
      name: 'searchbar',
      component: searchbar
    }, 
    {
      path: '/user-avatar',
      name: 'useravatar',
      component: useravatar
    },
    {
      path: '/topbar-setting',
      name: 'topbar-setting',
      component: topbarsetting 
    },
    {
      path: '/topbar-notification',
      name: 'topbar-notification',
      component: topbarnotification
    }     
  ]
})
